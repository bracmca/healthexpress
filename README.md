Install Guide
=============

# Prerequisites #

You must have the following packages installed on your system

* **Node.js**
* **NPM (*node package manager*)**

If you don't have the above installed please follow this [link](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions).

# Install Required Node Packages #

To install the required node packages for the application use the following commands.


```
#!shell

# add sudo if you want to install the packages system wide
npm install -g ionic cordova bower

```

# Clone the project #

If you are using ssh for cloning the project then please make sure that you have a private ssh rsa key and its added to your bitbucket account. If not you may find these links helpful - [bitbucket tutorial](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html), [github tutorial](https://help.github.com/articles/generating-ssh-keys/)

*(I'm assuming you keep all your work in a directory such as - ~/workspace)*

```
#!shell

cd ~/workspace

# clone using ssh
git clone git@bitbucket.org:bracmca/healthexpress.git
```

Clone using https, you'll be prompted to enter your bitbucket username and password. Please provide them accordingly

```
#!shell

# clone using https
git clone https://redmoses@bitbucket.org/bracmca/healthexpress.git
```

# Install Project Dependencies #

To install the project dependencies follow the below steps -


```
#!shell

# change into the directory where you've cloned the project
cd ~/workspace/healthexpress

# install the npm dependencies
npm install

# install the bower dependencies
bower install

```

# Run the application #

To run the application,


```
#!shell

# change into the project directory
cd ~/workspace/healthexpress

# run the project
ionic serve

```