'use strict';

angular.module('healthexpress')

  .controller('DoctorCtrl', function ($scope, $ionicModal, $ionicLoading, FirebaseService, AuthenticationService, $state, $stateParams, $location) {

    $scope.newDoctor = {};
    var limit = 5;

    $scope.startLoading = function (text) {
      $ionicLoading.show({
        template: '<p>' + text + '</p><ion-spinner></ion-spinner>'
      });
    };

    $scope.stopLoading = function () {
      $ionicLoading.hide();
    };

    $scope.listDoctors = function (refresh) {
      $scope.startLoading('Loading doctors...');
      var fbdocs = FirebaseService.paginate('doctors', limit);
      fbdocs.$loaded(function (data) {
        if (data) {
          $scope.doctors = data;
        }
        $scope.stopLoading();
      }, function (error) {
        if (error) {
          console.error('Error:', error);
        }
        $scope.stopLoading();
      });

      if (refresh) {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$apply();
      }
    };

    $scope.loadMore = function () {
      // get the last item
      if ($scope.doctors) {
        var lastDoctor = $scope.doctors[$scope.doctors.length - 1];
        if (lastDoctor) {
          var moreDoctors = FirebaseService.paginate('doctors', limit + 1, lastDoctor.$id);
          moreDoctors.$loaded(function (data) {
            if (data) {
              for (var i = 1; i < data.length; i++) {
                $scope.doctors.push(data[i]);
              }
              $scope.$broadcast('scroll.infiniteScrollComplete');
            }
          }, function (error) {
            if (error)
              console.error('Error:', error);
          });
        }
      }
    };

    $scope.$on('$stateChangeSuccess', function () {
      $scope.loadMore();
    });

    $scope.showDoc = function (id) {
      $state.go('tab.doctor-detail', {
        'doctorId': id
      });
    };

    $scope.showFacility = function (facility) {
      $state.go('tab.facility-detail', {
        'facilityId': facility.id,
        'facilityType': facility.type,
        'star': $scope.star($scope.rand()),
        'distance': ($scope.doctor.$id * .7 || .5).toFixed(1) + 'Km away'
      });
    };

    // rating
    $scope.rand = function () {
      return Math.floor((Math.random() * 5) + 1);
    };

    $scope.star = function (n) {
      return '★★★★★'.slice(-n);
    };

    $scope.loadDoc = function () {
      $scope.startLoading('Loading doctor...');
      var fbdoctor = FirebaseService.get('doctors', $stateParams.doctorId);
      fbdoctor.$loaded(function (data) {
        if (data) {
          $scope.doctor = data;
          $scope.doctor.about = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ' +
            'Integer ultrices dui elit, rhoncus interdum nulla finibus id. ' +
            'Sed posuere lectus nec euismod consectetur. Interdum et.';
        }
        $scope.stopLoading();
      }, function (error) {
        if (error)
          console.error('Error:', error);
        $scope.stopLoading();
      });
    };

    $scope.reset = function () {
      $scope.newDoctor.name = '';
      $scope.newDoctor.email = '';
      $scope.newDoctor.visitingHours = '';
    };

    $scope.addDoctor = function () {
      console.log($scope.newDoctors);
      $scope.doctors.$add({
        name: $scope.newDoctor.name,
        email: $scope.newDoctor.email,
        visitingHours: $scope.newDoctor.visitingHours
      }).then(function (error) {
        if (error)
          console.error('Error:', error);
      });
      $scope.reset();
      $scope.modal.hide();
    };

    $scope.removeDoctor = function (doctor) {
      $scope.doctors.$remove(doctor);
    };

    $scope.updateDoctor = function () {
      console.log($scope.doctor);
      $scope.doctor.$save().then(function (ref) {
        if (ref.key() === $scope.doctor.$id)
          $scope.modal.hide();
      }, function (error) {
        console.error('Error:', error);
      });

    };

  });
