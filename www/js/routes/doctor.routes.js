'use strict';

angular.module('healthexpress')
  .config(function($stateProvider) {
    $stateProvider
      .state('tab.doctors', {
        url: '/doctors',
        views: {
          'tab-doctors': {
            templateUrl: 'templates/doctors/list.html',
            controller: 'DoctorCtrl'
          }
        }
      })
      .state('tab.doctor-detail', {
        url: '/doctors/:doctorId',
        views: {
          'tab-doctors': {
            templateUrl: 'templates/doctors/detail.html',
            controller: 'DoctorCtrl'
          }
        }
      });
  });
